<!DOCTYPE html>
<html lang="pt-Br">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Lineage II - Old Sea</title>

    @include('System.Core.css')
  </head>
  <body id="page-top">

    @include('System.Core.Components._navbar')
    @yield('content')

    @include('System.Core.js')
    @yield('js-section')

  </body>
</html>