    <!-- Bootstrap core JavaScript -->
    <script src="/l2j-brasil/vendor/jquery/jquery.min.js"></script>
    <script src="/l2j-brasil/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/l2j-brasil/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/l2j-brasil/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="/l2j-brasil/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/l2j-brasil/js/creative.js"></script>